import React, { useContext, useState, useEffect } from 'react';
import UserContext, { UserProvider } from '../../UserContext'
import View from '../../components/View';
import {Row , Col, InputGroup, Container} from 'react-bootstrap'
import {Pie} from 'react-chartjs-2'
import {colorRandomizer} from '../../helper'

export default function index() {
    return ( 
        <View title={ 'Budget Tracker' }>
            <Row className="justify-content-center">
                <Col xs md="12">
                    <h3>BreakDown</h3>
                    <PieChart /> 
                </Col>
            </Row>
        </View>
    )
}

const PieChart = () => {
    const { user } = useContext(UserContext);
    const [startDate, setStartDate] = useState("")
    const [endDate, setEndDate] = useState ("")
    const [amount, setAmount] = useState ("")
    const [name, setName] = useState("")
    const [bgColors, setBgColors] = useState("")
    const [dataSets, setDataSets] = useState ("")
    console.log(user.record)

    useEffect(() => {
            console.log(user.record)
            if( startDate !== "" && endDate !==""){

                setDataSets(user.record.filter(records => {
                    return (
                        records.createdOn >= startDate && records.createdOn <= endDate   
                    )
                }))
                
            }              
    },[startDate, endDate])

    console.log(dataSets)

    useEffect(()=>{
        if(dataSets){
            setAmount (dataSets.map(records => records.amount ))
            setName ( dataSets.map(records => records.categoryName))
            setBgColors(dataSets.map(()=>`#${colorRandomizer()}`));
            
        }
        
    },[dataSets])
    
    const data = {
        labels : name,
        datasets: [{
            data: amount,
            backgroundColor:bgColors,
            hoverBackgroundColor:bgColors
            
        }]     
    }
    




    return(
        <React.Fragment>
            <Container> 
                <InputGroup>

                    <h6>Start Date: <input type= "date" value={startDate} onChange={e => setStartDate(e.target.value)} /></h6>
                    
                    <h6>End Date: <input type= "date" value= {endDate} onChange={e => setEndDate(e.target.value)} /> </h6>

                </InputGroup>

                <Pie 
                data = {data}
                width = {100}
                height = {100}
                 />

            </Container>
             
        
        </React.Fragment>
            
    )
}


